#!/usr/bin/env python

# meterMonitor.py
# 2023-11-27
# Public Domain

import sys
import time
import pigpio
import pickle
import os
from datetime import datetime
import logging
from logging.handlers import RotatingFileHandler
import yaml
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import threading

last  = [None]*32
cb    = []
meter_objs    = dict()

with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)

loglevel = config['loglevel'].upper()
numeric_level = getattr(logging, loglevel, None)
if not isinstance(numeric_level, int):
    raise ValueError('Invalid log level: %s' % loglevel)
logger = logging.getLogger('simple_logger')
logger.setLevel(numeric_level)
loghandler = RotatingFileHandler('meter.log', maxBytes=5000000, backupCount=10)
loghandler.setLevel(numeric_level)
logformatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
loghandler.setFormatter(logformatter)
logger.addHandler(loghandler)

idb_cfg = config['influxdb']

client = influxdb_client.InfluxDBClient(
   url   = idb_cfg['url'],
   token = idb_cfg['token'],
   org   = idb_cfg['org']
)

def end_str_with(s,e):
    if not s[-1] == e:
        s = s+e
    return s

class pickleStore:
    def __init__(self, filepath:str):
        self.filepath: str   = filepath
        self.ctime:    float = 0

    def load(self):
        if self.exists():
            with open(self.filepath, 'rb') as file:
                plo = pickle.load(file)
            return plo
        else:
            return None

    def dump(self, obj):
        with open(self.filepath, 'wb') as file:
            pickle.dump(obj, file)

    def exists(self):
        if os.access(self.filepath, mode=os.F_OK):
            if os.path.getsize(self.filepath) == 0:
                logger.warning("I found an empty file: '"+self.filepath+"'. It shall be removed.")
                os.remove(self.filepath)
                return False
            self.ctime = os.stat(self.filepath).st_ctime
            return True
        return False

class twoWireMeter:
    def __init__(self, nr, name, reading:float=0, khf:int=10, klf:int=1):
        self.nr:          str   = nr
        self.name:        str   = name
        self.step:        int   = khf*klf
        self.count:       int   = reading*1000  # keep track of count in litres
        self.start:       int   = reading
        self.reporttime:  float = 0

    def increase_by_one(self):
        self.count += self.step

    def read(self): # in cubic meters
        return self.read_l()/1000

    def read_l(self): # in litres
        self.reporttime = datetime.now().timestamp()
        return int(self.count)

    def set_count(self, reading):
        self.count = reading*1000

class readingFileMonitor:
    def __init__(self, path:str):
        self.path:     str   = path
        self.readtime: float = 0
        self.ctime:    float = 0
        if os.access(path, mode=os.F_OK):
            self.ctime = os.stat(path).st_ctime

    def exists(self):
        if os.access(self.path, mode=os.F_OK):
            self.ctime = os.stat(self.path).st_ctime
            return True
        else:
            return False

    def changed(self):
        if self.exists():
            return self.ctime > self.readtime
        else:
            return False

    def reading(self):
        if self.exists():
            with open(self.path, 'r') as file:
                reading = float(file.readline().strip())
            logger.info("reported reading is "+str(reading))
            self.readtime = datetime.now().timestamp()
            logger.debug("setting read time to "+str(self.readtime))
            return reading
        else:
            return 0

class meterMonitor:
    def __init__(self, nr:str, name:str, amdtFilePath:str, prsstFilePath:str, khf:int=10, klf:int=1):
        self.amdt_mon = readingFileMonitor(amdtFilePath)
        self.schloss  = threading.Lock()
        self.pickle   = pickleStore(prsstFilePath)
        start         = self.amdt_mon.reading()
        if self.pickle.exists() and  self.pickle.ctime > self.amdt_mon.ctime:
            logger.debug("Found persistence file for meter "+name+". I will count from then on.")
            self.meter = self.pickle.load()
        else:
            self.meter = twoWireMeter(nr, name, start, khf, klf)

    def increase_counter(self):
        self.meter.increase_by_one()

    def read_counter(self):
        return self.meter.read() # in cubic meters

    def file_changed(self):
        return self.amdt_mon.changed()

    def file_reading(self):
        return self.amdt_mon.reading()

    def set_counter(self, reading):
        self.meter.set_count(reading) # in cubic meters
        self.send_reading_metric(method='manual')

    def update_reading_as_reported(self):
        self.set_counter(self.file_reading())

    def amend_time(self):
        return self.amdt_mon.ctime

    def last_meter_time(self):
        return self.meter.reporttime

    def idb_point(self, meas, fields={'value':True}, tags=dict()):
        alltags = idb_cfg['tags'].copy()
        alltags.update(tags)
        alltags.update(
                {'name': self.meter.name,
                 'nr':   self.meter.nr}
                )
        p = influxdb_client.Point.from_dict(
                {'measurement': meas,
                 'tags':        alltags,
                 'fields':      fields}
                )
        return p

    def idb_reading_point(self):
        p   = self.idb_point("meter_reading", fields={"value": self.read_counter()})
        return p

    def idb_beat_point(self, q):
        tagset = {'vol':      self.meter.step,
                  'category': 'apsa-vars'}
        p   = self.idb_point("flow_beat", fields={'value':q}, tags=tagset)
        return p

    def send_metric(self, p, tags=dict()):
        for t in tags:
            p = p.tag(t, tags[t])
        write_api = client.write_api(write_options=SYNCHRONOUS)
        write_api.write(bucket=idb_cfg['bucket'], org=idb_cfg['org'], record=p)
        logger.debug("I just sent this metric:\n"+p.to_line_protocol())

    def send_reading_metric(self, method):
        tags = {'method':   method,
                'category': 'apsa-vars'}
        p = self.idb_reading_point()
        self.send_metric(p, tags)

    def send_beat_metric(self, diff): # diff in seconds
        q = self.meter.step/diff # flow approximation [l/s]
        p = self.idb_beat_point(q)
        self.send_metric(p)

    def persist_load(self):
        meter = None
        if self.pickle.exists():
            self.schloss.acquire()
            meter = self.pickle.load()
            self.schloss.release()
        return meter

    def persist_dump(self):
        self.schloss.acquire()
        meter = self.pickle.dump(self.meter)
        self.schloss.release()

def constWrapper_meterMonitor(GPIO):
    amdt_filepath     = end_str_with(config['amend_path'],'/')       + str(GPIO)
    persist_filepath  = end_str_with(config['persistence_path'],'/') + str(GPIO)
    klf  = config['meters'][GPIO]['klf']
    khf  = config['meters'][GPIO]['khf']
    nr   = config['meters'][GPIO]['m_nr']
    name = config['meters'][GPIO]['name']
    mon  = meterMonitor(nr, name, amdt_filepath, persist_filepath, klf, khf)
    return mon

def cbf(GPIO:int, level:int, tick):
    global config, meter_objs, last
    if last[GPIO] is not None:
        diff = pigpio.tickDiff(last[GPIO], tick)
        if meter_objs[GPIO].pickle.exists():
            meter_objs[GPIO].increase_counter()
            meter_objs[GPIO].send_beat_metric(diff/(10**6)) # pgpio tick difference in seconds
        else:
            meter_objs[GPIO].persist_dump()
        logger.debug( "G={} l={} d={:>8} r={}"
                       .format(GPIO, level, diff, meter_objs[GPIO].read_counter()) )
    last[GPIO] = tick

pi = pigpio.pi()
if not pi.connected:
    message = "could not connect to pigpiod"
    logger.critical(message)
    print("\n"+message)
    exit()

def make_sure_dir_exists(path):
    if not os.access(path, mode=os.F_OK):
        logger.warning("Path does not exist. Creating path "+path)
        os.makedirs(path, exist_ok=True)

persist_path = config['persistence_path']
amend_path   = config['amend_path']
make_sure_dir_exists(persist_path)
make_sure_dir_exists(amend_path)

G = list(config['meters'].keys())
for g in G:
    logger.info("trying to config pin "+str(g))
    pi.set_pull_up_down(g, pigpio.PUD_DOWN)
    cb.append(pi.callback(g, pigpio.FALLING_EDGE, cbf))
    meter_objs[g] = constWrapper_meterMonitor(g)

try:
    while True:
        for key in meter_objs:
            if meter_objs[key].pickle.exists():
                if meter_objs[key].file_changed():
                    meter_objs[key].update_reading_as_reported()
                else:
                    meter_objs[key].send_reading_metric(method='auto')
                meter_objs[key].persist_dump()
        time.sleep(60)

except KeyboardInterrupt:
    message = "Tidying up"
    logger.info(message)
    print("\n"+message+"\n")
    for c in cb:
        c.cancel()

pi.stop()

