service_name="meter-monitor"
script_file="meterMonitor.py"
config_file="config.yaml"

echo "[Unit]
Description=Meter monitor for water meters with cyble sensors (or reed switch sensors)
[Service]
Type=simple
User=$USER
Group=$USER
WorkingDirectory=$(pwd)
ExecStart=$(pyenv which python) $(readlink -f $script_file) -c $config_file

[Install]
WantedBy=multi-user.target" > $service_name.service
# WantedBy=multi-user.target" > /usr/lib/systemd/system/$service_name.service

echo "enable $service_name" > 20-$service_name.preset
# echo "enable $service_name" > /usr/lib/systemd/system-preset/20-$service_name.preset

echo "Generated files for daemonizing this script.
You can put them in place by issuing these commands:

sudo cp $service_name.service /usr/lib/systemd/system/$service_name.service
sudo cp 20-$service_name.preset /usr/lib/systemd/system-preset/20-$service_name.preset"
